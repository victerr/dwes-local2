<?php


namespace DWES\app\controllers;

use DWES\app\entity\Grupo;
use DWES\app\repository\GrupoRepository;
use DWES\core\exceptions\ValidationException;

class GrupoController
{
    public function listar()
    {
        $grupoRepository = new GrupoRepository();

        $grupos = $grupoRepository->findAll();

        include __DIR__ . '/../views/grupos.view.php';
    }

    public function nuevo()
    {
        $grupoRepository = new GrupoRepository();

        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            if (!isset($_POST['nombre']) || empty($_POST['nombre']))
                throw new ValidationException('El campo nombre no se puede quedar vacío');

            $nombre = $_POST['nombre'];

            $grupo = new Grupo();
            $grupo->setNombre($nombre);

            $grupoRepository->save($grupo);

            $mensaje = "El grupo se ha insertado correctamente";
        }

        include __DIR__ . '/../views/grupos.view.php';

    }
}