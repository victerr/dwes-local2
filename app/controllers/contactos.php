<?php

use DWES\core\App;
use DWES\app\repository\GrupoRepository;
use DWES\app\repository\ContactoRepository;

try
{
    $id = $_GET['id'] ?? null;
    $grupos = App::getRepository(GrupoRepository::class)->findAll();

    $contactoRepository = App::getRepository(ContactoRepository::class);
    $contactos = $contactoRepository->findAll();
}
catch(Exception $exception)
{
    $error = $exception->getMessage();
}

include __DIR__ . '/../views/contactos.view.php';
