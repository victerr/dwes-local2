<?php

use DWES\app\entity\Contacto;
use DWES\core\exceptions\ValidationException;
use DWES\core\App;
use DWES\app\repository\ContactoRepository;

if (!isset($_POST['nombre']) || empty($_POST['nombre']))
    throw new ValidationException('El campo nombre no se puede quedar vacío');
if(!isset($_POST['telefono']) || empty($_POST['telefono']))
    throw new ValidationException('El campo teléfono no se puede quedar vacío');
if(!isset($_POST['grupo']) || empty($_POST['grupo']))
    throw new ValidationException('El campo grupo no se puede quedar vacío');

$id = $_POST['id'];
/**
 * @var Contacto $contacto
 */
$contacto = App::getRepository(ContactoRepository::class)->find($id);

$nombre = $_POST['nombre'];
$telefono = $_POST['telefono'];
$grupo = $_POST['grupo'];
$idGrupoAnterior = $contacto->getGrupo();

$contacto->setNombre($nombre);
$contacto->setTelefono($telefono);
$contacto->setGrupo($grupo);

App::getRepository(ContactoRepository::class)->edita($contacto, $idGrupoAnterior);

$mensaje = "El contacto se ha editado correctamente";

App::get('router')->redirect('contactos');