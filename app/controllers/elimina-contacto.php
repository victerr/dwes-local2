<?php


use DWES\core\App;
use DWES\app\repository\ContactoRepository;
use DWES\core\helpers\MyMailer;

if (isset($_GET['id']) && !empty($_GET['id'])) {
    $contactoRepository = App::getRepository(ContactoRepository::class);

    $id = $_GET['id'];
    $contacto = $contactoRepository->find($id);
    $contactoRepository->elimina($contacto);
    App::getService(MyMailer::class)->sendEmail($contacto->getNombre());

    App::get('router')->redirect('contactos');
}