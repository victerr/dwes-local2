<?php
use DWES\core\App;
use DWES\app\repository\GrupoRepository;

if (isset($_GET['id']) && !empty($_GET['id']))
{
    $grupoRepository = App::getRepository(GrupoRepository::class);

    $id = $_GET['id'];
    $grupo = $grupoRepository->find($id);
    $grupoRepository->elimina($grupo);

    App::get('router')->redirect('grupos');
}