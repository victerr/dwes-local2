<?php
use DWES\app\repository\GrupoRepository;
use DWES\core\exceptions\ValidationException;
use DWES\app\entity\Grupo;

try
{
    $grupoRepository = new GrupoRepository();

    if ($_SERVER['REQUEST_METHOD'] === 'POST')
    {
        if (!isset($_POST['nombre']) || empty($_POST['nombre']))
            throw new ValidationException('El campo nombre no se puede quedar vacío');

        $nombre = $_POST['nombre'];

        $grupo = new Grupo();
        $grupo->setNombre($nombre);

        $grupoRepository->save($grupo);

        $mensaje = "El grupo se ha insertado correctamente";
    }

    $grupos = $grupoRepository->findAll();
}
catch(Exception $exception)
{
    $error = $exception->getMessage();
}

include __DIR__ . '/../views/grupos.view.php';
