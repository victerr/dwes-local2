<?php

use DWES\app\entity\Contacto;
use DWES\core\exceptions\ValidationException;
use DWES\app\utils\File;
use DWES\core\helpers\MyLogger;
use DWES\core\App;
use DWES\app\repository\ContactoRepository;

if (!isset($_POST['nombre']) || empty($_POST['nombre']))
    throw new ValidationException('El campo nombre no se puede quedar vacío');
if(!isset($_POST['telefono']) || empty($_POST['telefono']))
    throw new ValidationException('El campo teléfono no se puede quedar vacío');
if(!isset($_POST['grupo']) || empty($_POST['grupo']))
    throw new ValidationException('El campo grupo no se puede quedar vacío');

$nombre = $_POST['nombre'];
$telefono = $_POST['telefono'];
$grupo = $_POST['grupo'];

$contacto = new Contacto();
$contacto->setNombre($nombre);
$contacto->setTelefono($telefono);
$contacto->setGrupo($grupo);

$file = new File(
    'foto',
    'uploads/',
    ['image/jpeg', 'image/png']
);

$file->uploadFile();

$foto = $file->getFileUrl();

$contacto->setFoto($foto);

App::getRepository(ContactoRepository::class)->nuevo($contacto);

$mensaje = "Se ha insertado correctamente el contacto con nombre $nombre";
App::getService(MyLogger::class)->addMessage($mensaje);

App::get('router')->redirect('contactos');