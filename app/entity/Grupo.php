<?php

namespace DWES\app\entity;

use DWES\app\entity\IEntity;

class Grupo implements IEntity
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $nombre;
    /**
     * @var int
     */
    private $numContactos;

    /**
     * Grupo constructor.
     */
    public function __construct()
    {
        $this->numContactos = 0;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Grupo
     */
    public function setId(int $id): Grupo
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Grupo
     */
    public function setNombre(string $nombre): Grupo
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumContactos(): int
    {
        return $this->numContactos;
    }

    /**
     * @param int $numContactos
     * @return Grupo
     */
    public function setNumContactos(int $numContactos): Grupo
    {
        $this->numContactos = $numContactos;
        return $this;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'numContactos' => $this->numContactos
        ];
    }
}