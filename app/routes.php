<?php

$router->get('', 'PagesController@inicio');
$router->get('contactos', 'ContactoController@listar');
$router->get('grupos', 'GrupoController@listar');
$router->post('grupos/nuevo', 'GrupoController@nuevo');
$router->get('grupos/eliminar', 'GrupoController@eliminar');
$router->get('contactos/eliminar', 'ContactoController@eliminar');
$router->post('contactos/nuevo', 'ContactoController@nuevo');
$router->post('contactos/editar', 'ContactoController@editar');
