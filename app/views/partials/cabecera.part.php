<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Agenda de contactos</title>
    <link type="text/css" rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
</head>
<body>
<header>
    <h1>Agenda de contactos</h1>
</header>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item <?= DWES\app\utils\Utils::isCurrentPage('inicio.php') ? 'active' : '';?>">
                <a class="nav-link" href="/">Inicio</a>
            </li>
            <li class="nav-item <?= DWES\app\utils\Utils::isCurrentPage('index.php') ? 'active' : '';?>">
                <a class="nav-link" href="/contactos">Contactos</a>
            </li>
            <li class="nav-item <?= DWES\app\utils\Utils::isCurrentPage('grupos.php') ? 'active' : '';?>">
                <a class="nav-link" href="/grupos">Grupos</a>
            </li>
        </ul>
    </div>
</nav>