<?php

use DWES\core\App;
use DWES\core\exceptions\AppException;

try
{
    $config = require __DIR__ . '/../app/config.php';

    App::bind('config', $config);

}catch (AppException $appException)
{
    die('Error: ' . $appException->getMessage() );
}
