<?php

namespace DWES\core\database;

use DWES\core\App;
use PDO;

class Connection
{
    /**
     * @return PDO
     * @throws AppException
     * @throws QueryException
     */
    public static function make() : PDO
    {
        try
        {
            $config = App::get('config')['database'];

            $pdo = new PDO(
                $config['connection'] . ';dbname=' . $config['dbname'],
                $config['dbuser'],
                $config['dbpassword'],
                $config['options']
            );
        }
        catch (PDOException $pdoException)
        {
            throw new QueryException("No se ha podido conectar a la BBDD. Error: " . $pdoException->getMessage());
        }

        return $pdo;
    }
}