<?php

namespace DWES\core\database;

use DWES\core\App;
use DWES\core\exceptions\QueryException;
use DWES\core\exceptions\NotFoundException;
use DWES\app\entity\IEntity;
use PDO;
use PDOException;

abstract class QueryBuilder
{
    /**
     * @var string
     */
    private $table;
    /**
     * @var string
     */
    private $classEntity;
    /**
     * @var PDO
     */
    private $connection;
    /**
     * @var bool
     */
    private $loadDataBeforeConstruct;

    /**
     * QueryBuilder constructor.
     * @param string $table
     * @param string $classEntity
     * @throws QueryException
     */
    public function __construct(string $table, string $classEntity, bool $loadDataBeforeConstruct)
    {
        $this->table = $table;
        $this->classEntity = $classEntity;
        $this->connection = App::getConnection();
        $this->loadDataBeforeConstruct = $loadDataBeforeConstruct;
    }

    private function getInsertSQL(array $params)
    {
        $claves = array_keys($params);
        $campos = implode(', ', $claves);
        $parametros = ':' . implode(', :', $claves);

        return sprintf(
            'insert into %s (%s) values (%s)',
            $this->table,
            $campos,
            $parametros
        );
    }

    public function save(IEntity $entity)
    {
        $params = $entity->toArray();
        $sql = $this->getInsertSQL($params);
        $pdoStatement = $this->connection->prepare($sql);

        return $pdoStatement->execute($params);
    }

    private function getUpdateSQL(array $params)
    {
        $sql = 'update ' . $this->table . ' set ';
        foreach ($params as $key=>$param)
        {
            if ($key !== 'id')
                $sql .= "$key =:$key, ";
        }

        $sql = trim($sql, ', ');
        $sql .= ' where id=:id';

        return $sql;
    }

    /**
     * @param IEntity $entity
     * @throws QueryException
     */
    public function update(IEntity $entity)
    {
        try{
            $params = $entity->toArray();
            $sql = $this->getUpdateSQL($params);

            $pdoStatement = $this->connection->prepare($sql);
            $pdoStatement->execute($params);
        }catch (PDOException $PDOException)
        {
            throw new QueryException(
                'Error al editar. ERROR: ' . $PDOException->getMessage());
        }
    }

    private function getFetchStyle()
    {
        $fetch_style = PDO::FETCH_CLASS;
        if ($this->loadDataBeforeConstruct === false)
            $fetch_style |= PDO::FETCH_PROPS_LATE;

        return $fetch_style;
    }
    /**
     * @return array
     * @throws QueryException
     */
    public function findAll() : array
    {
        try{
            $sql = 'SELECT * from ' . $this->table;

            $pdoStatement = $this->connection->prepare($sql);

            $pdoStatement->execute();

            $entities = $pdoStatement->fetchAll($this->getFetchStyle(), $this->classEntity);

            return $entities;
        }catch (PDOException $PDOException)
        {
            throw new QueryException(
                'No se han podido obtener los contactos de la BBDDD. ERROR: ' . $PDOException->getMessage());
        }
    }

    /**
     * @param int $id
     * @return IEntity
     * @throws NotFoundException
     * @throws QueryException
     */
    public function find(int $id) : IEntity
    {
        try{
            $sql = 'SELECT * FROM ' . $this->table . ' WHERE id=:id';

            $pdoStatement = $this->connection->prepare($sql);
            $pdoStatement->bindParam('id', $id);
            $pdoStatement->execute();

            $entities = $pdoStatement->fetchAll(
                $this->getFetchStyle(), $this->classEntity);

            if (count($entities) === 1)
                return $entities[0];
            else
                throw new NotFoundException("Elemento con id $id no encontrado");
        }catch (PDOException $PDOException)
        {
            throw new QueryException(
                'No se han podido obtener los contactos de la BBDDD. ERROR: ' . $PDOException->getMessage());
        }
    }

    public function remove(IEntity $entity)
    {
        try{
            $id = $entity->getId();
            $sql = 'DELETE FROM ' . $this->table . ' WHERE id=:id';

            $pdoStatement = $this->connection->prepare($sql);
            $pdoStatement->bindParam('id', $id);
            $pdoStatement->execute();
        }catch (PDOException $PDOException)
        {
            throw new QueryException(
                'Error al eliminar. ERROR: ' . $PDOException->getMessage());
        }
    }

    public function executeTransaction(callable $fnTransaction)
    {
        try
        {
            $this->connection->beginTransaction();

            $fnTransaction();

            $this->connection->commit();
        }catch (PDOException $PDOException)
        {
            $this->connection->rollBack();

            throw new QueryException(
                'Error al ejecutar la transacción. ERROR: ' . $PDOException->getMessage());
        }
    }
}