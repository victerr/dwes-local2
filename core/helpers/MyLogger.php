<?php

namespace DWES\core\helpers;

use DWES\core\App;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class MyLogger
{
    /**
     * @var \Monolog\Logger
     */
    private $log;

    /**
     * MyLogger constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $config = App::get('config')['logger'];
        $this->log = new Logger('log-dwes');
        $this->log->pushHandler(
            new StreamHandler($config['directory'], Logger::INFO)
        );

    }

    /**
     * @param string $message
     */
    public function addMessage(string $message)
    {
        $this->log->info($message);
    }
}