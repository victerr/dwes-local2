<?php

use DWES\core\App;
use DWES\core\Router;
use DWES\core\Request;
use DWES\core\exceptions\ValidationException;
use DWES\core\exceptions\AppException;

try
{
    require 'vendor/autoload.php';
    require 'core/bootstrap.php';

    $router = Router::load(__DIR__ . '/app/routes.php');

    App::bind('router', $router);

    $router->direct(Request::uri(), Request::method());


}catch(AppException $appException)
{
    die('Error: ' . $appException->getMessage());
}catch(ValidationException $validationException)
{
    die('Error: ' . $validationException->getMessage());
}
